<?php

namespace Drupal\config_auto_export;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\CachedStorage;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigImporterEvent;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\DestructableInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\language\Config\LanguageConfigOverrideCrudEvent;
use Drupal\language\Config\LanguageConfigOverrideEvents;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Config subscriber.
 */
class ConfigSubscriber implements EventSubscriberInterface, DestructableInterface {

  /** @var \Drupal\Core\Config\ImmutableConfig */
  protected $config;

  /** @var \Drupal\Core\Config\CachedStorage */
  protected $configCache;

  /** @var \Drupal\Core\Config\FileStorage */
  protected $configStorage;

  /** @var array */
  protected $configSplitFiles;

  /** @var bool */
  protected $active = TRUE;

  /** @var bool */
  protected $triggerNeeded = FALSE;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new Settings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Config\CachedStorage $config_cache
   * @param \Drupal\Core\Config\FileStorage $config_storage
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(ConfigFactoryInterface $config_factory, CachedStorage $config_cache, FileStorage $config_storage, FileSystemInterface $file_system, LoggerChannelFactoryInterface $logger_channel_factory, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->config = $config_factory->get('config_auto_export.settings');
    $this->configCache = $config_cache;
    $this->configStorage = $config_storage;
    $this->fileSystem = $file_system;
    $this->logger = $logger_channel_factory->get('config');
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * @return bool
   *   Protected function enabled.
   */
  protected function enabled(): bool {
    static $enabled;

    if (!isset($enabled)) {
      $enabled = FALSE;
      if ($this->config->get('enabled')) {
        $uri = $this->config->get('directory');
        if ($this->fileSystem->prepareDirectory($uri, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
          if (is_writable($uri) || @chmod($uri, 0777)) {
            $enabled = TRUE;
          }
        }
      }
    }

    return $enabled;
  }

  /**
   * Trigger the webhook.
   */
  protected function trigger() {
    $webhook = $this->config->get('webhook');
    if (empty($webhook)) {
      return;
    }
    $exportPath = $this->fileSystem->realpath($this->config->get('directory'));
    $configPath = $this->fileSystem->realpath(config_get_config_directory(CONFIG_SYNC_DIRECTORY));
    $data = [
      'form_params' => Yaml::decode(str_replace(
        ['[export directory]', '[config directory]'],
        [$exportPath, $configPath],
        $this->config->get('webhook_params'))),
    ];

    try {
      $client = new Client(['base_uri' => $webhook]);
      $client->request('post', '', $data);
    }
    catch (GuzzleException $e) {
      $this->logger->critical('Trigger for config auto export failed: {msg}', ['msg' => $e->getMessage()]);
    }
  }

  /**
   * Read all config files from config splits, if available.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function readConfigSplitFiles() {
    $this->configSplitFiles = [];
    if (!$this->moduleHandler->moduleExists('config_split')) {
      return;
    }
    $extension = '.yml';
    $regex = '/' . str_replace('.', '\.', $extension) . '$/';
    foreach ($this->entityTypeManager->getStorage('config_split')->loadMultiple() as $split) {
      /** @noinspection AdditionOperationOnArraysInspection */
      $this->configSplitFiles += file_scan_directory($split->get('folder'), $regex, ['key' => 'filename']);
    }
    ksort($this->configSplitFiles);
  }

  /**
   * @param string $name
   *
   * @return bool
   */
  protected function existsInConfigSplit($name): bool {
    if (!isset($this->configSplitFiles)) {
      try {
        $this->readConfigSplitFiles();
      } catch (InvalidPluginDefinitionException $e) {
      } catch (PluginNotFoundException $e) {
      }
    }
    return isset($this->configSplitFiles[$name . '.yml']);
  }

  /**
   * Saves changed config to a configurable directory.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   Public function onConfigSave event.
   */
  public function onConfigSave(ConfigCrudEvent $event) {
    if ($this->active && $this->enabled()) {
      $name = $event->getConfig()->getName();
      if ($this->existsInConfigSplit($name)) {
        return;
      }
      $this->configStorage->write($name, $this->configCache->read($name));
      $this->triggerNeeded = TRUE;
    }
  }

  /**
   * Saves changed config translation to a configurable directory.
   *
   * @param \Drupal\language\Config\LanguageConfigOverrideCrudEvent $event
   *   Public function onConfigTranslationSave event.
   */
  public function onConfigTranslationSave(LanguageConfigOverrideCrudEvent $event) {
    if ($this->active && $this->enabled()) {
      /** @var \Drupal\language\Config\LanguageConfigOverride $object */
      $object = $event->getLanguageConfigOverride();
      $configLanguageStorage = $this->configStorage->createCollection('language.' . $object->getLangcode());
      $configLanguageStorage->write($object->getName(), $object->get());
      $this->triggerNeeded = TRUE;
    }
  }

  /**
   * Turn off this subscriber on importing configuration.
   *
   * @param \Drupal\Core\Config\ConfigImporterEvent $event
   *   Public function onConfigImportValidate event.
   */
  public function onConfigImportValidate(ConfigImporterEvent $event) {
    $this->active = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::SAVE][] = ['onConfigSave', 0];
    $events[ConfigEvents::IMPORT_VALIDATE][] = ['onConfigImportValidate', 1024];
    if (class_exists(LanguageConfigOverrideEvents::class)) {
      $events[LanguageConfigOverrideEvents::SAVE_OVERRIDE][] = ['onConfigTranslationSave', 0];
    }
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function destruct(): void {
    if ($this->triggerNeeded) {
      $this->trigger();
    }
  }

}
