Stop Broken Link In Body
------------------------

The Stop broken link in body module detects defunct links from your content
right when the content is being created.
While user creates a node, the module detects broken links in the body by 
checking the remote sites and evaluating the HTTP response codes. It shows all
broken links in the content's body section and on the content edit page, if a 
link check fails. If a broken link is needed, the content cannot be saved.


Installation:
-------------
1. Install Stop Broken Link In Body via Modules page.
2. Go to Modules and enable the "Stop broken link in body" module.
3. Go to Configuration -> System -> Stop broken link in body.
4. Select Content type which you want to stop broken link in body.
5. Save configuration.
