# CKEDitor Datetime
This module provides a CKEditor plugin to insert [a time element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/time) into the WYSIWYG using a datepicker.

## Dependencies
[DateJS](https://github.com/datejs/datejs) allows JavaScript date formatting using PHP format strings.



## Installation

### Using Composer
1. Include the following lines in the `repositories` section
 of your project's `composer.json`:

        {
            "type": "package",
            "package": {
                "name": "datejs/datejs",
                "version": "0.0.0",
                "type": "drupal-library",
                "source": {
                    "url": "https://github.com/datejs/datejs.git",
                    "type": "git",
                    "reference": "origin/master"
                }
            }
        }
2. Run `composer require datejs/datejs`

### Without Composer
1. Download the DateJS library from https://github.com/datejs/datejs
2. Extract it into your site's `web/libraries` directory
3. Rename the directory to `datejs`, so that the JS file is at `/libraries/datejs/build/date.js`

## Configuration
1. Go to /admin/config/content/formats
2. Edit your editor configuration:
    1. Add the Datetime button to your toolbar
    2. If you use the "limit allowed HTML tags" filter, allow `<time datetime>`
