<?php

namespace Drupal\ckeditor_datetime\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "datetime" plugin.
 *
 * NOTE: The plugin ID ('id' key) corresponds to the CKEditor plugin name.
 * It is the first argument of the CKEDITOR.plugins.add() function in the
 * plugin.js file.
 *
 * @CKEditorPlugin(
 *   id = "datetime",
 *   label = @Translation("Date time")
 * )
 */
class DateTime extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   *
   * NOTE: The keys of the returned array corresponds to the CKEditor button
   * names. They are the first argument of the editor.ui.addButton() or
   * editor.ui.addRichCombo() functions in the plugin.js file.
   */
  public function getButtons() {
    // Make sure that the path to the image matches the file structure of
    // the CKEditor plugin you are implementing.
    return [
      'Datetime' => [
        'label' => t('Datetime'),
        'image' => drupal_get_path('module', 'ckeditor_datetime') . '/js/plugins/datetime/icons/datetime.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    // Make sure that the path to the plugin.js matches the file structure of
    // the CKEditor plugin you are implementing.
    return drupal_get_path('module', 'ckeditor_datetime') . '/js/plugins/datetime/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
      'core/jquery',
      'core/jquery.ui.datepicker',
      'ckeditor_datetime/datejs',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $config = [];
    $patterns = [];

    // Get all the available date formats.
    $formats = \Drupal::entityTypeManager()
      ->getStorage('date_format')
      ->loadMultiple();

    foreach ($formats as $format) {
      $patterns[$format->id()] = $format->getPattern();
    }

    $config['datetime_formats'] = $patterns;

    $format = \Drupal::config('ckeditor_datetime.settings')->get('default_format');

    $config['default_format'] = $format;

    return $config;
  }

}
