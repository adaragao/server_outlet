/**
 * @file
 * CKEditor Datetime plugin.
 */

(function ($, Drupal, CKEDITOR) {
  CKEDITOR.plugins.add("datetime", {
    icons: "datetime",
    beforeInit: function beforeInit(editor) {
      editor.on('widgetDefinition', function (event) {
        var widgetDefinition = event.data;
        widgetDefinition.dateFormats = editor.config.datetime_formats;
      });

      editor.addCommand("datetime", new CKEDITOR.dialogCommand("datetimePicker"));

      editor.ui.addButton("Datetime", {
        label: "Insert \<time\> element",
        command: "datetime",
        toolbar: "insert"
      });

      CKEDITOR.dialog.add("datetimePicker", function (editor) {

        // Get the list of PHP date formats from Drupal.
        var formatList = editor.config.datetime_formats;
        var dateFormats = [];
        var defaultDateFormat = "";

        // Convert the format for JS, and add to the option list.
        for (var property in formatList) {
          if (formatList.hasOwnProperty(property)) {
            var phpFormat = formatList[property];
            var thisReadableFormat = property + " - " + phpFormat;
            var jsFormat = Date.normalizeFormat(phpFormat);
            dateFormats.push([thisReadableFormat, jsFormat]);

            if (property === editor.config.default_format) {
              defaultDateFormat = jsFormat;
            }
          }
        }

        return {
          title: "Select date",
          minWidth: 400,
          minHeight: 200,
          contents: [
            {
              id: "general",
              elements: [
                {
                  type: "text",
                  id: "selectedTime",
                  label: "Time:",
                  onShow: function () {
                    // Convert the input to a time input, because CKEditor
                    // doesn't seem to support it.
                    var dialogId = "#" + this.domId
                    var inputId = "#" + jQuery(dialogId).find("input").attr("id");

                    var $timeInput = jQuery(inputId);

                    $timeInput.attr("type", "time");

                    // Changing the type means the input loses styling, so re-add it.
                    $timeInput.css({
                      "border": "1px solid #b8b8b8",
                      "border-radius": "2px"
                    });

                    // CKEditor wraps things in tables - make this row half-width.
                    $timeInput.closest("tr").css({
                      "width": "50%",
                      "display": "inline-block",
                    });
                  }
                },
                {
                  // Add a text input to store the date.
                  type: "text",
                  id: "selectedDate",
                  label: "Date:",
                  onShow: function () {

                    var dialogId = "#" + this.domId
                    var inputId = "#" + jQuery(dialogId).find("input").attr("id")

                    // CKEditor wraps things in tables - make this row half-width.
                    jQuery(inputId).closest("tr").css({
                      "width": "50%",
                      "display": "inline-block",
                      "vertical-align": "top"
                    });

                    // Apply the datepicker to the input control.
                    jQuery(dialogId).datepicker({
                      changeMonth: true,
                      changeYear: true,
                      dateFormat: "yy-mm-dd",
                      showButtonPanel: true,
                      altField: inputId
                    });
                  }
                },
                {
                  id: "dateFormat",
                  type: "select",
                  label: "Date Format",
                  items: dateFormats,
                  default: defaultDateFormat
                }
              ]
            }
          ],
          onOk: function () {
            var dateFormat = this.getValueOf("general", "dateFormat");

            // Default to midnight.
            var selectedTime = "00:00";

            // If a time is selected, show the time.
            var timeValue = this.getValueOf("general", "selectedTime");
            if (!!timeValue) {
              selectedTime = timeValue;
            }

            // Get the selected date from the datepicker.
            var dateValue = this.getValueOf("general", "selectedDate");

            // TODO: deal with time zones.
            var selectedDate = new Date(dateValue + "T" + selectedTime);
            var machineDate = selectedDate.toISOString();

            // Format the date using DateJS.
            console.log(dateFormat);
            var humanDate = selectedDate.toString(dateFormat);

            console.log(humanDate);

            editor.insertHtml('<time datetime="' + machineDate + '">' + humanDate + '</time>')
          }
        }
      })
    }
  })

  CKEDITOR.plugins.datetime = {

  };

})(jQuery, Drupal, CKEDITOR);
