CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Create and Continue module creates a button on the node edit form that
allows a user to create a node and continue to create another node of that type
upon saving.

It is a utility module to save time while creating content.

 * For a full description of the module visit:
   https://www.drupal.org/project/createandcontinue

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/createandcontinue


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Create and continue module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. There is no configurarion neccessary. Each node now has a "Save and add
       another" button on the buttom of the node edit form.


MAINTAINERS
-----------

 * Dominique De Cooman (domidc) - https://www.drupal.org/u/domidc
 * Stijn Stroobants (StijnStroobants) - https://www.drupal.org/u/stijnstroobants
