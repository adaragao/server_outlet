<?php

namespace Drupal\Tests\cryptolog\Functional;

use PHPUnit\Framework\SkippedTestError;

/**
 * Tests Cryptolog with the Redis contributed module.
 *
 * @group cryptolog
 * @requires module redis
 */
class CryptologRedisTest extends CryptologTest {

  /**
   * {@inheritdoc}
   */
  protected $eventId = 2;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    if (extension_loaded('redis')) {
      $this->container->get('module_installer')->install(['redis']);
    }
    else {
      throw new SkippedTestError('Required PHP extension: redis');
    }
  }

}
