# Portuguese, Portugal translation of Entity Clone (8.x-1.0-beta3)
# Copyright (c) 2019 by the Portuguese, Portugal translation team
#
msgid ""
msgstr ""
"Project-Id-Version: Entity Clone (8.x-1.0-beta3)\n"
"POT-Creation-Date: 2019-06-04 02:34+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Language-Team: Portuguese, Portugal\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

msgid "Label"
msgstr "Etiqueta"
msgid "@type"
msgstr "@type"
msgid "Clone"
msgstr "Clonar"
msgid "About"
msgstr "Sobre"
